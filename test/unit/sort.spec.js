var sort = require('../../src/libs/sort');
var expect = require('chai').expect;

describe('Array', function () {
    describe('sort', function () {
        var array, length = 1000;

        beforeEach(function () {
            array = [];

            while (array.length < length) {
                array.push(Math.random());
            }
        });

        it('ascending', function () {
            sort(array);

            var success = true;
            for (var i = 0; i < array.length - 1; i++) {
                success = success && array[i] <= array[i + 1];
            }

            expect(success).to.be.true;
        });

        it('descending', function () {
            sort(array, true);

            var success = true;
            for (var i = 0; i < array.length - 1; i++) {
                success = success && array[i] >= array[i + 1];
            }

            expect(success).to.be.true;
        });
    });
});