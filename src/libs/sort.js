(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        module.exports = factory();
    } else if (typeof require === 'function' && typeof define === 'function') {
        define(function () {
            return factory();
        });
    } else {
        window.sort = factory();
    }
}(function () {
    return function (array, desc) {
        var temp, n = array.length;
        for (var i = 0; i < n - 1; i++) {
            for (var j = 0; j < n - i - 1; j++) {
                if (desc ? array[j] < array[j + 1] : array[j] > array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }
}));