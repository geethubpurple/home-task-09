require.config({
    baseUrl: './js',
    paths: {
        'jquery': './libs/jquery',
        'backbone': './libs/backbone',
        'backbone.marionette': './libs/backbone.marionette',
        'underscore': './libs/underscore',
        'localstorage': './libs/backbone.localstorage'
    }
});

require([
    'jquery',
    'backbone',
    './application/Application',
    './root/Router',
    './products/Router',
    './cart/Router',
    './order/Router'
], function ($, Backbone, Application, RootRouter, ProductsRouter, CartRouter, OrderRouter) {

    var app = new Application();

    new RootRouter;
    new ProductsRouter({
        container: app.layout.content
    });
    new CartRouter({
        container: app.layout.content
    });
    new OrderRouter({
        container: app.layout.content
    });

    $(function () {
        app.on('start', function () {
            Backbone.history.start();
        });

        app.start();
    })
});
